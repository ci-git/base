GemUI 0.4 pour Pure Data et Gem
===============================

Pure Data (ou abr�g� pd) est un logiciel de programmation graphique pour la cr�ation musicale et multim�dia en temps r�el.

GEM signifie "Graphics Environment for Multimedia" (en fran�ais "environnement graphique pour le multim�dia"). Elle a �t� �crite par Mark Danks pour la production d'images en temps r�el, particuli�rement pour les compositions audiovisuelles. Elle permet la manipulation d'objet de synth�se, d'images et de vid�os via l'utilisation d'OpenGL.

___

GemUI est une s�rie d'abstractions pour l'environnement Pure Data et gem. Celles-ci proposent des objets graphiques de bases (toggle, hslider, knob...) permettant de r�aliser des interfaces graphique personnalisables directement dans gem.

Elle inclu �galement un gestionnaire de fen�tre gem [gem_window], facilitant la prise en main de gem:
- Cr�ation de fen�tre avec diff�rents param�tres (taille, position, framerate, lumi�re, couleur etc...).
- Gestion de la cam�ra � la souris permutable avec la touche shift. (clic gauche = angle cam�ra, clic droit = zoom).
- Affichage d'une grille de rep�res renseignant sur les coordonn�es spatiales utilis�es par gem, [translateXYZ]...



Installation:
-------------

- Note: il est conseill� d'utiliser la derni�re version de Pure Date extended disponible � cette adresse:

===============================
http://puredata.info/downloads
===============================

Ainsi que la derni�re version de gem disponible ici:

===========
gem.iem.at
===========



- D�compressez l'archive sur votre disque, puis lancez le patch "_START_GemUI.pd". Les liens de ce dernier patch vous permettrons d'explorer toutes les abstractions de la librairie GemUI. chaque objet poss�de son fichier d'aide.


Fonctionnement:
-------------

1- les outlets de [gem_window] et [gem_mouse] permettent de r�cup�rer ind�pendament le clic gauche, centre ou droit de la
souris pour chaque objet. Cela permet par exemple de superposer des interfaces sous
forme de couche tout en manipulant certaines d'entre-elles.


2- Les objets graphiques s'utilisent tous de la mani�re suivante:

[type_objet nom_objet positionX positionY largeur hauteur]

exemple:  [gem_toggle 2 1 0.5 0.5]



3- les objets de test utilisent un autre syst�me de coordonn�e,
adapt� suivant le type de forme test�, voir fichier d'aide. (clic droit sur un objet => help)



===============================
http://raphael.isdant.free.fr
===============================

bug et autres propositions:

raphael.isdant@free.fr

// variables

diam = 50;
hauteur = 5;
dollarFN = 100;
diviseur = 12;

module gorge (rayon, gorgeFN, diviseur) {
	rotate_extrude(convexity = 10, $fn = gorgeFN)
	translate([rayon, 0, 0])
	circle(r = rayon/diviseur, $fn = gorgeFN );
}

module disque(rayon, epaisseur, rayon_axe, lissage ) {
 	// rayon_axe correspond au diamètre du noyau de la vis Mx
	// cf https://fr.wikipedia.org/wiki/Filetage_m%C3%A9trique
	
	marge= 0.1;
	$fn= lissage ;

	difference() {
		cylinder( r=rayon , h = epaisseur, center = true);	
		
		#cylinder(r=rayon_axe, h = epaisseur+marge, center = true);	
		}
}

//////////////// version pleine  sans poignée à décommenter

/* 
difference() {
disque(diam/2, hauteur, 2.5 , dollarFN );
#gorge (diam/2, dollarFN, diviseur);
}

*/

//////////////// version allégée avec poignée
/*
difference() {
	union(){
		disque(diam/2, hauteur, 2.5 , dollarFN );
		translate([(diam/2)-hauteur*2,hauteur*3,0])
		cylinder(r = hauteur /2 , h = hauteur * 4, $fn = dollarFN);
	}
	
	translate([0,-diam/4,-hauteur])
	#cylinder($fn=6, r=hauteur, h = hauteur*2);
	translate([-diam/4,0,-hauteur])
	#cylinder($fn=6, r=hauteur, h = hauteur*2);
	translate([0,diam/4,-hauteur])
	#cylinder($fn=6, r=hauteur, h = hauteur*2);
	translate([diam/4,0,-hauteur])
	#cylinder($fn=6, r=hauteur, h = hauteur*2);
	#gorge (diam/2, dollarFN, diviseur);
	
}
*/
//////////////// version allégée sans poignée

difference() {
	
	disque(diam/2, hauteur, 2.5 , dollarFN );

	translate([0,-diam/4,-hauteur])
	#cylinder($fn=6, r=hauteur, h = hauteur*2);
	translate([-diam/4,0,-hauteur])
	#cylinder($fn=6, r=hauteur, h = hauteur*2);
	translate([0,diam/4,-hauteur])
	#cylinder($fn=6, r=hauteur, h = hauteur*2);
	translate([diam/4,0,-hauteur])
	#cylinder($fn=6, r=hauteur, h = hauteur*2);

	#gorge (diam/2, dollarFN, diviseur);
	
}
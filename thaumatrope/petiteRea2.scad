// variables

// diam = 50;
hauteur = 5;
dollarFN = 100;
marge = 1;
diam = 30;

module gorge (rayon, gorgeFN) {
	rotate_extrude(convexity = 10, $fn = gorgeFN)
	translate([rayon, 0, 0])
	circle(r = rayon/7, $fn = gorgeFN );
}

module disque(rayon, epaisseur, rayon_axe, lissage ) {
 	// rayon_axe correspond au diamètre du noyau de la vis Mx
	// cf https://fr.wikipedia.org/wiki/Filetage_m%C3%A9trique
	marge= 0.1;
	$fn= lissage ;

difference() {
	cylinder(r=rayon, h = epaisseur, center = true);	
	#cylinder(r=rayon_axe, h = epaisseur+marge, center = true);
		

}
}

///////// version pleine à décommenter

/*
difference() {

disque(diam/2, hauteur, 2.5 , dollarFN );
#gorge (diam/2, dollarFN);
}
*/

///////// version allégée à décommenter

difference() {
	disque(diam/2, hauteur, 2.5 , dollarFN );
	#gorge (diam/2, dollarFN);
	translate([0,(-diam/4)*1.05,-hauteur])
	#cylinder($fn=6, r=hauteur*0.75, h = hauteur*2);
	translate([(-diam/4)*1.05,0,-hauteur])
	rotate([0,0,90])
	#cylinder($fn=6, r=hauteur*0.75, h = hauteur*2);
	translate([0,(diam/4)*1.05,-hauteur])
	#cylinder($fn=6, r=hauteur*0.75, h = hauteur*2);
	translate([(diam/4)*1.05,0,-hauteur])
	rotate([0,0,90])
	#cylinder($fn=6, r=hauteur*0.75, h = hauteur*2);
}
///////////        variables      ////////////////////////

h = 75;
e = 5;
l = 15;
percage = 2.5;
$fn=50;
marge=0.1;

////////////// modules ////////////////////////

module montants(hauteur, epaisseur, largeur) {
	
	translate([0,-e-marge*10,0])
	cube([hauteur,epaisseur,largeur], center=true);
	translate([0,e+marge*10,0])
	cube([hauteur,epaisseur,largeur], center=true);
	


}

/* 
*/	
	
difference() {
	union(){
		montants (h,e,l);
		rotate([0,0,90])
		translate([0,h/2,0])
		
		// l'embase avec 2 prépercages

		difference(){
			cube([h*0.75,e,l], center=true);

			rotate([90,0,0])
			translate([l+l/3,0,-l/2])
			#cylinder(r=percage, h = l) ;

			rotate([90,0,0])
			translate([(-l-l/3),0,-l/2])
			#cylinder(r=percage, h = l) ;
		}
	}

	// le percage de l'axe des reas
	rotate([90,0,0])
	translate([(h/2)-e*2 , 0 ,-l/2-marge*10])
	#cylinder(r=percage, h = l*2) ;
}



#! /bin/sh
#  attention ce n'est plus le patch VDMX mini qui est lancé, juste pour les répés sans les écrans latéraux
: << COMMENTBLOCK
# lundi 15  fevrier 2010
# script pour lancer le système audio vidéo du spectacle Corps de femme 2
# pour la plateforme macintosh dans un premier temps
# copyright Olivier Heinry (olivier _AT_ heinry _DOT_ fr)
   This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
COMMENTBLOCK
# on commence par tuer les instances de pd qui tournent encore
ps -e | grep VDMX 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill VDMX!"
    echo
    killall -9 VDMX
fi
ps -e | grep MaxMSP 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill MaxMSP!"
    echo
    killall -9 MaxMSP
fi
ps -e | grep pdextended 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill Puredata!"
    echo
    killall -9 pd
fi
# on tue ensuite les �ventuelles instances de xterm
ps -e | grep xterm 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill xterm"
    echo
    killall -9 xterm
fi
#on envoie cash les diff�rentes instances, c'est pas grave on a 4 cores
cd ~/oli44-code-base/cdf;
echo "Startup directory: " $PWD 
echo "Starting Max/MSP..."; 
open ~/audio/MABEL_RUGBY_SON/CDF2.4.maxpat  &
echo "Starting VDMX..."; 
/Applications/devTools/VDMX5_b7.5.8/VDMX.app/Contents/MacOS/VDMX ~/oli44-code-base/cdf/monoplayer.vdmx &
#echo "Starting LC-Edit"
#open ~/oli44-code-base/cdf/light/cdf2-mini.lcp &
echo "Starting the Pd gui..."; 
/Applications/Pd-extended.app/Contents/Resources/bin/pd -noaudio -open main-cdf2.pd  &
echo "Starting the right Pd v-model"
# /Applications/Pd-extended.app/Contents/Resources/bin/pd -stderr -nomidi -noaudio -open v-model.pd -send "preset-file ../video-presets/right-mini.coll;" 2> /tmp/right &
echo "Starting the left Pd v-model";
# /Applications/Pd-extended.app/Contents/Resources/bin/pd -stderr -nomidi -noaudio -open v-model.pd -send "preset-file ../video-presets/left-mini.coll;" 2> /tmp/left & 
echo "Starting the Pd gui..."; 
# echo "Starting the Pd Lanbox gui..."; 
# /Applications/Pd-extended.app/Contents/Resources/bin/pd -noaudio -open light-cdf2.pd &
echo "Fin du script"

#! /bin/sh
# mardi 1er novembre 2011
# script pour lancer le syst�me audio vid�o du spectacle Corps de femme 3
# pour la plateforme MacOS 10.5
# copyright Olivier Heinry (olivier _AT_ heinry _DOT_ fr)
<< COMMENTBLOCK
   This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
COMMENTBLOCK
echo "Let's start Corps de femmes 3"
# on commence par tuer les instances de pd et max/msp qui tournent encore
ps -e | grep pdextended 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill Puredata!"
    echo
    killall -9 pd
fi
<< COMMENTBLOCK
ps -e | grep MaxMSP 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill Max/MSP too!"
    echo
    killall -9 MaxMSP
fi
COMMENTBLOCK
# on tue ensuite les eventuelles instances de xterm de CDF2
ps -e | grep xterm 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill xterm"
    echo
    killall -9 xterm
fi
# on recree ensuite les fichiers xterm de CDF3
# cd /tmp; rm left right cdf3-big cdf3-ground;
# touch cdf3-big; touch cdf3-ground;
cd ~/oli44-code-base/cdf;
# on envoie les differentes instances pour profiter du multicore 
# et multi fenetres GEM
echo "Startup directory: " $PWD
echo "Starting the big CDF3 Pd v-model"
/Applications/Pd-extended.app/Contents/Resources/bin/pd -nomidi -noaudio -open v6-model.pd -send "preset-file ../video-presets/cdf3-big.coll;" &
echo "Starting the ground Pd v-model";
/Applications/Pd-extended.app/Contents/Resources/bin/pd -nomidi -noaudio -open v-model.pd -send "preset-file ../video-presets/cdf3-ground.coll;" &
echo "Starting the Max/MSP sound patch..."; 
#chargement du patch max modifie
#open ~/audio/CDF3_SONS_MUSIQUES/CDF3_GENERAL_2.4.maxpat  &
open ~/audio/CDF3_SONS_MUSIQUES/CDF3_GENERAL_2.5.maxpat  &
echo "Starting the Pd gui... and the Make Controller helper"; 
/Applications/Pd-extended.app/Contents/Resources/bin/pd -noaudio -open main-cdf3.pd cysp2/mchelper.pd &
#echo "Starting the xterm logs..."
# xterm -title "CDF3 big log" -geometry +496+540 -e tail -f /tmp/cdf3-big &
# xterm -title "CDF3 ground log" -geometry +0+540 -e tail -f /tmp/cdf3-ground &
echo "Fin du script"

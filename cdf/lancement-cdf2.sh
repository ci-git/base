#! /bin/sh
# lundi 15  f�vrier 2010
# script pour lancer le système audio vid�o du spectacle Corps de femme 2
# pour la plateforme macintosh dans un premier temps
# copyright Olivier Heinry (olivier _AT_ heinry _DOT_ fr)
: << COMMENTBLOCK
   This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
COMMENTBLOCK
ps -e | grep VDMX 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill VDMX!"
    echo
    killall -9 VDMX
fi
ps -e | grep MaxMSP 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill MaxMSP!"
    echo
    killall -9 MaxMSP
fi
ps -e | grep pdextended 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill Puredata!"
    echo
    killall -9 pd
fi
# on tue ensuite les éventuelles instances de xterm
ps -e | grep xterm 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill xterm"
    echo
    killall -9 xterm
fi
#on envoie cash les differentes instances, c'est pas grave on a 4 cores
cd /tmp; rm left right ;
touch left; touch right;
cd ~/oli44-code-base/cdf;
echo "Startup directory: " $PWD 
echo "Starting Max/MSP..."; 
open ~/audio/MABEL_RUGBY_SON/CDF2.4.maxpat  &
echo "Starting VDMX..."; 
/Applications/devTools/VDMX5_b7.5.8/VDMX.app/Contents/MacOS/VDMX ~/oli44-code-base/cdf/monoplayer.vdmx &
echo "Starting the right Pd v-model"
/Applications/Pd-extended.app/Contents/Resources/bin/pd -nomidi -noaudio -open v-right-model.pd -send "preset-file ../video-presets/right.coll;" &
echo "Starting the left Pd v-model";
/Applications/Pd-extended.app/Contents/Resources/bin/pd -stderr -nomidi -noaudio -open v-left-model.pd -send "preset-file ../video-presets/left.coll;" & 
echo "Starting the Pd gui..."; 
/Applications/Pd-extended.app/Contents/Resources/bin/pd -noaudio -open main-cdf2.pd  &
echo "no more starting the xterm logs..."
echo "Fin du script"

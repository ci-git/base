#! /bin/sh
# mardi 1er novembre 2011
# script pour lancer le syst�me audio vid�o du spectacle Corps de femme 3
# pour la plateforme MacOS 10.5
# copyright Olivier Heinry (olivier _AT_ heinry _DOT_ fr)
<< COMMENTBLOCK
   This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
COMMENTBLOCK
echo "Let's start Corps de femmes 3"
# on commence par tuer les instances de pd et max/msp qui tournent encore
ps -e | grep pd-extended 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill Puredata!"
    echo
    killall -9 pd-extended
fi
# on tue ensuite les �ventuelles instances de xterm de CDF2
ps -e | grep xterm 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill xterm"
    echo
    killall -9 xterm
fi
# on recree ensuite les fichiers xterm de CDF3
# cd /tmp; rm left right cdf3-big cdf3-ground;
# touch cdf3-big; touch cdf3-ground;
cd ~/oli44-code-base/cdf;
# on envoie les differentes instances pour profiter du multicore 
# et multi fenetres GEM
echo "================= Startup directory: " $PWD
echo "================= Starting the big CDF3 Pd v-model"
#pd-extended -nomidi -noaudio -open v6-model.pd -send "preset-file ../video-presets/cdf3-big.coll;" &
pd-extended -nrt -nomidi -noaudio -open v6-model.pd -send "preset-file ../video-presets/cdf3-big-linux.coll;" &
sleep 5s; echo "====================== Starting the ground Pd v-model";
pd-extended -nrt -nomidi -noaudio -open v-model.pd -send "preset-file ../video-presets/cdf3-ground-linux.coll;" &
sleep 3s;
echo "==================  Starting the Pd gui..."; 
pd-extended -nrt -jack -open main-cdf3-linux.pd cysp2/mchelper.pd &
sleep 1s;
echo "====================== Starting the Pd sound patch..."; 
pd-extended -rt -jack -r 48000 -open a-model.pd &
echo "================= Fin du script ===================="

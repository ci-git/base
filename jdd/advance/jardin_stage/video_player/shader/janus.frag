uniform float hue;
uniform float sat;
uniform float light;

const vec3 LumCoeff = vec3 (0.2125, 0.7154, 0.0721);

uniform vec3 RGBcoeff ;

uniform vec3 avgluma ;
uniform float saturation;
uniform float contrast;
uniform float brightness;


uniform float freq;
uniform float lineSize;
uniform float offset;
uniform float alphaFx;
uniform int swFx;


uniform float alpha;


varying vec2 texcoord0;
varying vec2 texdim0;

uniform sampler2DRect tex0 ;

float Hue_2_RGB(float v1, float v2, float vH )
{
	float ret;
   if ( vH < 0.0 )
     vH += 1.0;
   if ( vH > 1.0 )
     vH -= 1.0;
   if ( ( 6.0 * vH ) < 1.0 )
     ret = ( v1 + ( v2 - v1 ) * 6.0 * vH );
   else if ( ( 2.0 * vH ) < 1.0 )
     ret = ( v2 );
   else if ( ( 3.0 * vH ) < 2.0 )
     ret = ( v1 + ( v2 - v1 ) * ( ( 2.0 / 3.0 ) - vH ) * 6.0 );
   else
     ret = v1;
   return ret;
}

vec4 hsl (vec4 color)
{

    float Cmax, Cmin;

    float D;  
    float H, S, L;
    float R, G, B;

    R = color.r;
    G = color.g;
    B = color.b;

// convert to HSL

    Cmax = max (R, max (G, B));
    Cmin = min (R, min (G, B));

// calculate lightness
    L = (Cmax + Cmin) / 2.0;

  if (Cmax == Cmin) // it's grey
  {
		H = 0.0; // it's actually undefined
		S = 0.0;
  }
  else
  {
    D = Cmax - Cmin; // we know D != 0 so we cas safely divide by it



// calculate Saturation
    if (L < 0.5)
    {
      S = D / (Cmax + Cmin);
    }
    else
    {
      S = D / (2.0 - (Cmax + Cmin));
    }

// calculate Hue
    if (R == Cmax)
    {
      H = (G - B) / D;
		} else {
      if (G == Cmax)
      {
      	 H = 2.0 + (B - R) /D;
      }
      else
      {
        H = 4.0 + (R - G) / D;
      }
   	}

    H = H / 6.0;


	}

	// modify H/S/L values
	H += hue;
	S += sat;
	L += light;

	if (H < 0.0)
	{
		H = H + 1.0;
	}
	
	// clamp H,S and L to [0..1]
	
	H = clamp(H, 0.0, 1.0);
	S = clamp(S, 0.0, 1.0);
	L = clamp(L, 0.0, 1.0);
	
	// convert back to RGB
	
	float var_2, var_1;
	
	if (S == 0.0)
	{
	   R = L;
	   G = L;
	   B = L;
	}
	else
	{
	   if ( L < 0.5 )
	   {
	   var_2 = L * ( 1.0 + S );
	   }
	   else
	   {
	   var_2 = ( L + S ) - ( S * L );
	   }
	
	   var_1 = 2.0 * L - var_2;
	
	   R = Hue_2_RGB( var_1, var_2, H + ( 1.0 / 3.0 ) );
	   G = Hue_2_RGB( var_1, var_2, H );
	   B = Hue_2_RGB( var_1, var_2, H - ( 1.0 / 3.0 ) );
	}
	
	 return vec4(R,G,B, color.a);
		
}

	
vec4 colorize (vec4 color){
	color.r *= RGBcoeff.r ;
	color.g *= RGBcoeff.g ; 
	color.b *= RGBcoeff.b ; 
	return color ; 
}

vec4 brcosa (vec4 color){
	vec4 fragColor ; 
	vec3 intensity = vec3 (dot(color.rgb, LumCoeff));
	fragColor.rgb = mix(intensity, color.rgb, saturation);
	fragColor.rgb = mix(avgluma, fragColor.rgb, contrast);
	fragColor.rgb *= brightness;
	fragColor.a = color.a ; 
	return fragColor ; 

}

vec4 linelizer(vec4 color)
{
	vec4 fragColor ;
	
	float x = mod(texcoord0.y+offset,freq);
	
	if ((x > lineSize*freq))		
	  	fragColor = color;
	else {
		fragColor.r = 1. - color.r;
		fragColor.g = 1. - color.g;
		fragColor.b = 1. - color.b;
		fragColor.a = color.a;
	}
	return fragColor ; 
} 

void main(void)
{
	vec4 color = texture2DRect(tex0, texcoord0);
	color = hsl (color);
	color = colorize (color);
	color = brcosa (color);
	if (swFx!=0)
		color = mix (color, linelizer(color), alphaFx);
	color.a = color.a*alpha ; 
	gl_FragColor = color;
}

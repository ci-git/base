#! /bin/sh
# Maio 2012
# script pour lancer le système audio vidéo du spectacle 
# Fotograma Completo Principal
# pour la plateforme Linux
# copyright Olivier Heinry (olivier _AT_ heinry _DOT_ fr)
: << COMMENTBLOCK
   This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
COMMENTBLOCK
killall -9 pd-extended;
# on commence par tuer les instances de pd qui tournent encore
ps -e | grep pdextended 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill Puredata!"
    echo
    killall -9 pd-extended;
fi
# on tue ensuite les éventuelles instances de xterm
ps -e | grep xterm 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill xterm"
    echo
    killall -9 xterm;
    killall -9 pd-extended;
fi

#on envoie cash les différentes instances, c'est pas grave on a 4 cores
cd /tmp; rm uno dos tres;
touch uno; touch dos; touch tres;
cd ~/oli44-code-base/fotograma;
echo "Startup directory: " $PWD
echo "Starting el video-jugador uno"
pd-extended -nrt -noaudio -nomidi -open channel1.pd &
#pd-extended -nrt -noaudio -nomidi -stderr -open channel1.pd 2> /tmp/uno &
echo "Starting el video-jugador dos";
pd-extended -nrt -noaudio -nomidi -open channel2.pd  & 
#pd-extended -nrt -noaudio -nomidi -stderr -open channel2.pd 2> /tmp/dos & 
echo "Starting el video-jugador tres";
#pd-extended -nrt -noaudio -nomidi -stderr -open channel3.pd 2> /tmp/tres & 
pd-extended -nrt -noaudio -nomidi -open channel3.pd  & 
echo "Starting la interfacia";
#pd-extended -nrt -noaudio -open gui.pd osc_martes_02.pd osc_martes_05.pd   & 
pd-extended -nrt -noaudio -open gui.pd & 
echo "Starting the xterm logs..."
#xterm -title "channel2 log" -geometry +496+640 -e tail -f /tmp/dos &
#xterm -title "channel1 log" -geometry +0+640 -e tail -f /tmp/uno &
#xterm -title "channel3 log" -geometry +992+640 -e tail -f /tmp/tres &
echo "Fin du script"

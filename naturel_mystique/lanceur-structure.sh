#! /bin/sh
# avril 2012
# script pour lancer le systeme audio video du spectacle Naturel Mystique
# copyright Olivier Heinry (olivier _AT_ heinry _DOT_ fr)
<< COMMENTBLOCK
   This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


# on commence par tuer les instances de pd  qui tournent encore

ps -e | grep pd-extended 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill Puredata!"
    echo
    killall -9 pd-extended
fi
COMMENTBLOCK
echo "/n/nLet's start Naturel mystique/n/n"
cd /home/olivier/oli44-code-base/naturel_mystique ;
# on envoie les differentes instances pour profiter du multicore 
# et multi fenetres GEM
echo "================= Startup directory: " $PWD
echo "================= Starting Pd v-model"
cd video;
pd-extended -nrt -nomidi -noaudio -nosleep -open v-model-w-cam.pd &
echo "==================  Starting the Pd gui..."; 
cd - ; 
pd-extended -nrt -jack -r 48000 -alsamidi -nosleep -open main.pd &
sleep 1s;
echo "====================== Starting the Pd sound patch..."; 
pd-extended -rt -jack -r 48000 -channels 2 -nosleep -open a-model.pd &
echo "================= Fin du script ===================="
# cd /home/olivier/bin/kinect/Sensebloom-OSCeleton-7307683 ; osceleton

#! /bin/sh
# avril 2012
# script pour lancer le systeme audio video du spectacle Naturel Mystique
# copyright Olivier Heinry (olivier _AT_ heinry _DOT_ fr)
<< COMMENTBLOCK
   This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
COMMENTBLOCK
echo "Naturel mystique en route pour la gratte augmentée à la kinect"
echo "on commence par NE PAS tuer les instances de pd  qui tournent encore"
cd ~/oli44-code-base/naturel_mystique ;
# on envoie les differentes instances pour profiter du multicore 
# et multi fenetres GEM
echo "================= Startup directory: " $PWD
# pd-extended -nrt -nomidi -noaudio -open v-model.pd -send "preset-file video-presets.coll;" &
#echo "==================  Starting the Pd gui..."; 
#pd-extended -rt -jack -r 48000 -alsamidi -open gratte/thalgratte.pd &
echo "====================== Starting the Pd sound patch..."; 
pd-extended -rt -jack -r 48000 -outchannels 8 -open sampler/sampler.pd &
echo "================= Fin du script ===================="

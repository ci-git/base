// Nicolas Courcelle 2013

uniform float K;
uniform sampler2D texture;
uniform sampler2D texture2;
varying vec2 texcoord;


void main (void)
{
		vec4 color=texture2D(texture, vec2(texcoord.x, texcoord.y));
		vec4 color2=texture2D(texture2, vec2(texcoord.x, texcoord.y));
		gl_FragColor = mix(color, color2, K);

	}

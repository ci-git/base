// Nicolas Courcelle 2013

uniform float K;
uniform sampler2D texture;
varying vec2 texcoord;


void main (void)
{
		vec4 color=texture2D(texture, vec2(texcoord.x, texcoord.y));
			color=abs(color-K);
		gl_FragColor=color;
	}

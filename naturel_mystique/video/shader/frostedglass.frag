// http://coding-experiments.blogspot.fr/2010/06/frosted-glass.html
// by  Agnius Vasiliauskas

uniform sampler2D tex;

float rand(vec2 co){
  return fract(sin(dot(co.xy ,vec2(92.,80.))) + 
                  cos(dot(co.xy ,vec2(41.,62.))) * 5.1);
}

void main()
{
  vec2 rnd = vec2(rand(gl_TexCoord[0].xy),rand(gl_TexCoord[0].xy));
  gl_FragColor = texture2D(tex, gl_TexCoord[0].xy+rnd*0.05);
}

// source: http://code.google.com/p/glslang-library/

varying vec2 texcoord;

void main (void)

{
    gl_Position = ftransform();
    texcoord    = vec2(gl_TextureMatrix[0] * gl_MultiTexCoord0);
}

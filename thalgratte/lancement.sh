#! /bin/sh
# lundi 15  février 2010
# script pour lancer le système audio de la performance thalgratte
# copyright Olivier Heinry (olivier _AT_ heinry _DOT_ fr)
: << COMMENTBLOCK
   This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
COMMENTBLOCK
# on commence par tuer les instances de pd qui tournent encore
ps -e | grep pdextended 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill Puredata!"
    echo
    killall -9 pd
fi
# on tue ensuite les éventuelles instances de xterm
ps -e | grep xterm 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill xterm"
    echo
    killall -9 xterm
fi

#on envoie cash les différentes instances
cd /tmp; rm *.pdlog;
touch serveur.pdlog; touch gratte.pdlog;
cd ~/oli44-code-base/thalgratte;
echo "Startup directory: " $PWD
echo "Starting the serveur Pd a-model"
pdextended -stderr -alsamidi -rt -jack -r 48000 -channels 8 serveur.pd 2> /tmp/serveur.pdlog &
sleep 5s; echo "Starting the gratte Pd a-model";
pdextended -stderr -alsamidi -rt -jack -r 48000 -inchannels 2 -outchannels 8 thalgratte.pd lididac.pd e-touille  2> /tmp/gratte.pdlog & 
sleep 6s;
echo "Starting Kluppe..."; 
kluppe thalgit.klp&
sleep 3s;
echo "Starting the xterm logs..."
xterm -title "Spat server log" -geometry +496+540 -e tail -f /tmp/serveur.pdlog &
sleep 1s;
xterm -title "Gratte/gitarre log" -geometry +0+540 -e tail -f /tmp/thalgratte.pdlog &
echo "Fin du script"

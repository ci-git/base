#! /bin/sh
# lundi 15  février 2010
# script pour lancer le système audio de la performance 74 minutes
# copyright Olivier Heinry (olivier _AT_ heinry _DOT_ fr)
: << COMMENTBLOCK
   This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
COMMENTBLOCK

# on lance le serveur jackd via le programme qjackctl qui tue lui-même les instances des applications que l'on s'apprête à lancer
# afin de les redémarrer avec les bons réglages
echo -e "\nLet's start the Jack audio server...\n"
/usr/bin/jackd -R -P89 -p128 -dalsa -r48000 -p64 -n2 -D -Chw:2 -Phw:2&
sleep 3s; echo -e "\nLet's cache into RAM the Pd files to speed things up later...\n";
mkdir /tmp/cacache; cp -r ~/pd-externals /usr/lib/pd ~/svn_oli /tmp/cacache; rm -rf /tmp/cacache;
echo -e "\nLet's start the pd patch...\n"
MYDATE=$(date +"%b-%d-%y-%H%M");echo "logfile: " $MYDATE.log

cd ~/svn_oli/74patches;
pd -stderr -rt -jack -r 48000 -channels 10 -alsamidi -mididev 1 routeur.pd ~/pd-externals/pdmtl/1.browser.pd 2> $MYDATE.log
sleep 15s qjackctl

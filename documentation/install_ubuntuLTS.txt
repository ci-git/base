
partitionnement
==============


Install
========

ubuntu LTS vanilla


applications
=============

ajout des depots apt.puredata.info  et tango

ajout des paquets suivants:

blender audacity ardour jackd qjackctl gedit-plugins pd-extended revelation sun-java6-bin -jre -fonts acroread adobe-flashplugin acceptation de la license? cacert vim-full python-setuptools
ajouter les greffons gstreamer mp3 timidity

code
====

subversion git-core easygit git-svn

ajout des depots svn_oli et afrovalise

svn co https://pingbase.homelinux.net/svn_oli
svn co https://pingbase.homelinux.net/afrovalise
gitorious

création du dossier pd-externals

svn  pdmtl mtl 

wget http://gridflow.ca/download/packages/gridflow-9.11-ubuntu-karmic-i386.tar.gz

svn co https://code.goto10.org/svn/pdmtl/trunk/mtl

configuration
=============

ajout du user dans le groupe audio
  usermod -a -G audio (null)
edition de /etc/security/limits.conf





#! /bin/sh
# lundi 15  février 2010
# script pour lancer le système audio de la performance 74 minutes
# copyright Olivier Heinry (olivier _AT_ heinry _DOT_ fr)
: << COMMENTBLOCK
   This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
COMMENTBLOCK

echo -e "\nLet's start the Jack audio server...\n"
cd ~/oli44-code-base/akef;
echo $PWD
pd-extended -rt -jack -r 48000 -channels 2 -nomidi audio_engine.pd &
pd-extended -rt -noaudio video_engine.pd akef_nyon_2010.pd &


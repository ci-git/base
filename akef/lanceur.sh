#! /bin/sh
# mai 2013
# script pour lancer le système du spectacle Qui a tué Ibrahim Akef?
# copyright Olivier Heinry (olivier _AT_ heinry _DOT_ fr)
: << COMMENTBLOCK
   This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
COMMENTBLOCK
# on tue ensuite les �ventuelles instances de xterm de CDF2
ps -e | grep pd-extended 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill pd-extended running instances"
    echo
    killall -9 pd-extended
fi

echo -e "\nLet's start the Jack audio server...\n"
cd ~/oli44-code-base/akef;
echo $PWD
pd-extended -rt -jack -r 48000 -channels 2 a-model.pd &
pd-extended -rt -jack -r 48000 -alsamidi main.pd &
pd-extended -nrt -noaudio -nomidi v-model-w-camera.pd -send "presets/big-linux.coll" &
pd-extended -nrt -noaudio -nomidi v-model.pd -send "presets/bas-linux.coll" &

/*
Projected Grid GLSL vertex shader v1.0
by Martins Upitis (martinsh) (devlog-martinsh.blogspot.com)
----------------------
The shader is Blender Game Engine ready, but it should be quite simple to adapt for your engine.

This work is licensed under a Creative Commons Attribution 3.0 Unported License.
So you are free to share, modify and adapt it for your needs, and even use it for commercial use.
I would also love to hear about a project you are using it.
----------------------
Have fun,
Martins
*/

/*
to get field of view (FOV) of camera, this is what I use:
sensor = 32 //image sensor area in mm
fov = 2*atan(sensor/(2*camera.lens))*180/pi //fov angle in degrees
near = zNear
far = zFar
width and height = your image dimensions
*/

uniform float fov, near, far, width, height;

const vec3 groundNormal = vec3(0.0,0.0,1.0); //facing up
const float groundHeight = 0.0;

vec3 intercept(vec3 lineP, vec3 lineN, vec3 planeN, float planeD)
{
	float distance = (planeD - dot(planeN, lineP)) /dot(lineN, planeN);
	return lineP + lineN * distance;
}

vec3 getViewPosition(vec2 coord)
{
	float aspect = (width/height);
	float scale = 1.0;
	if(width>height)
	{
		scale = aspect;
	}   
	vec3 pos;
	pos = vec3(coord.s*aspect*tan(fov/2.0),coord.t*tan(fov/2.0),tan(fov/2.0)*scale);
	return pos;
}

vec4 getWorldPosition(vec2 coord)
{
	vec3 view = getViewPosition(coord);
	vec4 world = vec4(view.x,view.y,-view.z,1.0);
	world = gl_ModelViewMatrixTranspose*world+gl_ModelViewMatrixInverse[3];
	return world;
}

vec4 getCamPosition(vec2 coord)
{
	vec3 view = getViewPosition(coord);
	vec4 world = vec4(view.x,view.y,-view.z,1.0);
	return gl_ModelViewMatrixTranspose*world;
}

void main()
{
	gl_TexCoord[0] = gl_MultiTexCoord0;

	vec3 campos = getCamPosition(gl_TexCoord[0].st).xyz;
	vec3 worldpos = getWorldPosition(gl_TexCoord[0].st).xyz;

	vec3 groundpos = intercept(worldpos, campos, groundNormal, groundHeight);
	vec4 groundviewpos = (gl_ModelViewProjectionMatrix*vec4(groundpos,1.0));

	float z = groundviewpos.z+2.0*near;
	float depth = ((far+near)/(far-near))+(1.0/z)*((-2.0*far*near)/(far-near));

	gl_Position = vec4(gl_Vertex.x,gl_Vertex.y,depth,1.0);
}
//https://www.shadertoy.com/view/XsfGzn

uniform vec3      iResolution;           // viewport resolution (in pixels)
uniform float     iGlobalTime;           // shader playback time (in seconds)
uniform float     iChannelTime[4];       // channel playback time (in seconds)
uniform vec3      iChannelResolution[4]; // channel resolution (in pixels)
uniform vec4      iMouse;                // mouse pixel coords. xy: current (if MLB down), zw: click
uniform samplerXX iChannel0..3;          // input channel. XX = 2D/Cube
uniform vec4      iDate;                 // (year, month, day, time in seconds)

// Created by inigo quilez - iq/2013
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.

void main(void)
{
	vec2 q = gl_FragCoord.xy / iResolution.xy;
    vec2 p = -1.0 + 2.0 * q;
    
	vec2 uv = 0.5 + 0.5*p;
	vec3 br = texture2D( iChannel1, uv ).xyz;

	uv = 0.5*vec2( length(p), sin(0.25*iGlobalTime+length(p))+atan( p.y, p.x )/3.1416 );
	vec3 fo = texture2D( iChannel0, uv ).xyz*2.0*length(p);
	
	
	br = mix( br, vec3( dot(br,vec3(0.33))), 0.3 );
	
    float maxrb = max( br.r,br.b);
    float k = clamp((br.g-maxrb)*4.0,0.0,1.0);
    br.g = min(br.g,maxrb*0.85);
    vec3 col = mix(br,fo,k);
	
	col *= vec3(1.0,0.9,1.0);
	
	
    col *= 0.25 + 0.75*pow( 16.0*q.x*q.y*(1.0-q.x)*(1.0-q.y), 0.25 );
	
	
    gl_FragColor = vec4(col, 1.0);
}


// philippe boisnard - 2013

uniform sampler2D colorTex;
uniform float X,Y,Z,A;

void main (void)
{
	vec4 alpha_color = texture2D(colorTex, gl_TexCoord[0].xy);
	
	if(alpha_color.r<X)
	{
		discard;
	}
if(alpha_color.g<Y)
	{
		discard;
	}
if(alpha_color.b<Z)
	{
		discard;
	}

vec4 color = alpha_color.rgba + (1.*A, 1.*A, 1.*A, 1.*A);
	
gl_FragColor  = texture2D(colorTex, gl_TexCoord[0].xy)+ color;
}

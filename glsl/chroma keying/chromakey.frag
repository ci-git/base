# http://stackoverflow.com/questions/4777063
# extension GL_ARB_texture_rectangle : enable

uniform sampler2DRect src_tex_unit0;
vec4 color;

void main(void)
{
    vec2 st=gl_TexCoord[o].st;
    vec4 sample=texture2DRect(src_tex_unit0,st);
    gl_FragColor=sample;
    if((sample.r > 0.5) && (sample.g < 0.5) && (sample.b > 0.5)) {
            gl_FragColor.a = 0.0;
            }
}

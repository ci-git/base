//

varying vec2 texcoord0;
varying vec2 texcoord1;

uniform vec4 in2scale;
uniform vec4 in2offset;

uniform sampler2D tex0;
uniform sampler2D tex1;

void main( void )
{
	vec4 v0 = texture2D(tex0, texcoord0);
	vec4 v1 = texture2D(tex1, texcoord1);
	
	v0 = (v0);	
	v1 = (v1*in2scale)+in2offset;	
	vec4 result = vec4(greaterThanEqual(v0,v1));
	gl_FragColor = (result);
}

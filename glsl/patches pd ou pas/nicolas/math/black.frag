uniform vec4 K;
uniform sampler2D texture;
varying vec2 texcoord;


void main (void)
{
		vec4 color=texture2D(texture, texcoord);
		float nb = ((color.r*K.r+color.g*K.g+color.b*K.b)/3.)*K.a;
		color =vec4(nb, nb, nb, 1.);
		gl_FragColor=color;
	}

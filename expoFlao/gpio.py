#! /usr/bin/env python
# -*- coding: utf-8 -*-

import time
import RPi.GPIO as io
io.setmode(io.BCM)

pir_pin = 18

io.setup(pir_pin, io.IN)    # on active l'entrée

def detection():
	if io.input(pir_pin):
		value = 1
	else:
		value = 0
        value = str(value) + "\n"
        print "value: "+ value
        return value

def writeToShm(value, filename):
	try:
		logfile = open(filename, "w")
		try: 
			logfile.write(str(value))
		finally:
			logfile.close()
	except IOError:
		pass
	
while True:
        fichier = "/run/shm/flamenco"
        contenu = detection()
	writeToShm(contenu, fichier)
	time.sleep(0.5)


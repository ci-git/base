// adapted from http://www.arduino.cc/playground/Main/RGBLEDPWM
const int motorPin = 9;
const int potPin = 2;
int currentValue;
void setup()
{
  pinMode(motorPin, OUTPUT);
}
void loop()
{
  currentValue = map (analogRead(potPin), 0, 1023, 255, 0);
  analogWrite(motorPin, currentValue);
}


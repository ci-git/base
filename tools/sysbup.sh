#! /bin/sh
# Script to backup the whole system neatly
/usr/bin/rdiff-backup --exclude /home --exclude /sys --exclude /tmp --exclude /mnt --exclude /proc --exclude /dev --exclude /cdrom --exclude /floppy --exclude /bup/rdiff / /bup/rdiff

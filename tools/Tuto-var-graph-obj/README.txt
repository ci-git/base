Tutorial management variables related to graphical objects in Pure Data

First, you need the software Pure Data > 0.40
Vanilla or pd-extended
Go there to download :
http://puredata.info/downloads

This tutorial is divided into 5 parts, part coresspond a patch.
please open one patch at a time and start with the first:
tuto1-srvar.pd

Description des patchs :
tuto1-srvar.pd - Use an abstraction to receive and send to and from a graphical element
tuto2-srvar.pd - Encapsulation of the first example 
tuto3-srvar.pd - Add preset's system - backup value of parameters
tuto4-srvar.pd - Add Midilearn's system - a simple way to use an external midi controller
tuto5-srvar.pd - Add min max - reduce an operating range of a parameter

Any questions :
thomas-thiery@th-th.fr

August 2009 - Thomas Thiery


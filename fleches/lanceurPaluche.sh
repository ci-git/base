#! /bin/sh
# Rev.  20121022
# script pour lancer le système video Paluche du spectacle Flèches du parthe
# copyright Olivier Heinry (olivier _AT_ heinry _DOT_ fr)
: << COMMENTBLOCK
   This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
COMMENTBLOCK
ps -e | grep pd-extended 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill Puredata!"
    echo
# on devrait d'abord tester l'OS pour connaitre le nom du programme que l'on veut tuer, car il varie légèrement d'une plate-forme à l'autre
    killall -9 pd-extended
fi
# on tue ensuite les éventuelles instances de xterm
cd ~/oli44-code-base/fleches;
echo "Startup directory: " $PWD 
echo "Starting the Pd controller"
pd-extended -nrt -noaudio -nomidi videoPaluche.pd &
echo "Fin du script"

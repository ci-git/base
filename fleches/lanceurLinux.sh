#! /bin/sh
# Rev.  20131126
# script pour lancer le système audio+video du spectacle Fleches du parthe
# copyright Olivier Heinry (olivier _AT_ heinry _DOT_ fr)
: << COMMENTBLOCK
   This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
COMMENTBLOCK
ps -e | grep pd-extended 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill Puredata!"
    echo
# on devrait d'abord tester l'OS pour connaitre le nom du programme que l'on veut tuer, car il varie légèrement d'une plate-forme à l'autre
    killall -9 pd-extended
fi
# on tue ensuite les éventuelles instances de xterm
cd ~/oli44-code-base/fleches;
echo "Startup directory: " $PWD 
echo "Starting the Pd controller"
pd-extended -nrt -noaudio gui.pd &
echo "Starting the Pd audio model";
#pd-extended -nomidi -jack -inchannels 0 -outchannels 2 -r 48000 -nrt audio-model.pd & 
pd-extended -nomidi -alsa -inchannels 0 -outchannels 2 -r 48000 audio-model.pd & 
echo "starting the Pd video model";
pd-extended -nrt -noaudio -nomidi video-model-ecran.pd &
echo "starting the Pd floor video model";
pd-extended -nrt -noaudio -nomidi video-model-murcour.pd &
echo "starting the Pd walls model";
pd-extended -nrt -noaudio -nomidi video-model-murjar.pd &
#L'horloge est desromais dans le patch audio
#pd-extended -rt -noaudio -nogui -nomidi horloge.pd &
echo "Fin du script"

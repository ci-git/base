#! /bin/sh
# mardi 27 mai 2008, 16:19:18 (UTC+0200)
# script pour lancer le système de caméra PTZ/Hid/Pd
# copyright Olivier Heinry (olivier _AT_ heinry _DOT_ fr)
: << COMMENTBLOCK
   This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
COMMENTBLOCK

# on tue toutes les instances des applications que l'on s'apprête à lancer
# afin de les redémarrer avec les bons réglages
#
#### Le code qui suit est emprunté à la rubrique bash punk d'apo33.org: Respect!
# on teste si pd tourne
ps -e|grep pd 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill Puredata!"
    echo
    killall -9 pd
fi

# on teste si le HID server tourne
ps x | grep hidserver.py 
if [ $? -eq 0 ]; then
	echo "Let's kill hidserver.py!"
	echo
	ps x | grep  python | grep hidserver.py | cut -f 1 -d ' ' | xargs kill
	if [ $? -eq 0 ]; then
		echo "Let's kill hidserver.py AGAIN!"
		echo
		ps x | grep  python | grep hidserver.py | cut -f 2 -d ' ' | xargs kill
	fi
fi

echo -e "\nJusqu'ici tout va bien...\nPlease check that the HID device is plugged, then press Enter.\n"
read

######### on peut passer au démarrage des apps

echo -e "\nLet's start the HID server!"
cd ~/pyscripts/libs/hidserver
python hidserver.py &
# on vérifie le lancement du hidserver
sleep 1s
echo
if [ $? != 0 ]; then
	echo -e "\nOhoooo! Something bad happened with our hidserver!\n"
	exit 1
else
	echo -e "Hidserver started...\n"
fi

echo -e "\nLet's start the Pd Gui and OSC parser!"
cd ~/orlosc/axis214
pd -nrt -noaudio -nomidi -stderr ~/orlosc/axis214/hidserver2gamepad.pd /usr/local/lib/pd/abs/pdmtl/1.browser.pd &

# on vérifie le lancement de Pd
sleep 1s
echo
if [ $? != 0 ]; then
	echo -e "\nOhoooo! Something bad happened the Puredata GUI!\n"
	exit 1
else
	echo -e "\nPuredata Gui started...\n"
fi
sleep 2s
# on démarre un  player video pour visionner le stream
vlc http://axis214/mjpg/video.mjpg &

# on quitte proprement en cas de bazar
if [ $? != 0 ]; then
	echo -e "\nOhoooo! Something bad happened.\n"
	exit 1
fi
exit

#! /bin/sh
# mercredi 14 mai 2008, 12:08:18 (UTC+0200)
# Author: Olivier 'chez' Heinry 'point' Fr
# Released under the GNU/GPL License
#

clear

echo
echo "Sets how long the system waits before executing any given action" 
echo
echo "Type in the command you wish to execute:"
echo 
read command
echo
echo "What time unit do you wish to use? [s]econds, [m]inutes, [h]ours or [d]ays?"
echo
read unit

# Next question is contextual

case $unit in
	s)echo "How many seconds do you wish to wait?"
	echo
	read var
	echo;;
	m)echo "How many minutes do you wish to wait?"
	echo
	read var
	echo;;
	h)echo "How many hours do you wish to wait?"
	echo
	read var
	echo;;
	d)echo "How many days do you wish to wait?"
	echo
	read var
	echo;;
	*) echo "illegal option $unit."
	exit 1
esac

echeance="${var}${unit}"
sleep $echeance
#placeholder for command"
$command
if [ $? = 0 ]; then
    exit
else
    exit 1
elif

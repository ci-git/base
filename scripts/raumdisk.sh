#!/bin/bash
# this script copies to RAM (to /dev/shm) a few files
# then symlinks them (ln -s).
# Author: Olivier Heinry ( olivier AT heinry DOT fr ) 
# Licensed under the Gnu GPL v2
# 2008-02-29

clear

### counting out the  variables

if $0<2
then
	echo "The syntax of the command reads the following: CMD source destination [symlinkname]"
	exit
else
	case $0 in
	2)	echo  "You're about to copy the folder $1 to RAM.";
		echo -n "Continue? y/n";
		read goon;
		case $goon in
			n) echo "exit";;
			N) exit;;
			y) cp -rv $1 /dev/shm$2;;
			Y) cp -rv $1 /dev/shm$2;;
			*) echo "Sorry I didn't understand your input"; exit;;
		esac;;
	# cp -rv $1 $2, ln -s $2 $3
	3) echo "nothing";;
elif	
src=$PWD/$src
echo "Source folder: "$src
### Saisie de la seconde variable
echo "zzz"
#du -ch $src 
echo "MMMMMM"
du -ch $src > grep total > dskn

### Manifestement tout ça ce n'est pas fini du tout!

$espacedisquenecessaire
df -h | grep /dev/shm > $espacedisquedisponible
echo "required disk space: $espacedisquenecessaire"
exit

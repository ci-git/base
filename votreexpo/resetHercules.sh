#! /bin/sh
device=$(uvcdynctrl -l | grep Hercules | awk '{print $1}')
echo "device:" $device
uvcdynctrl -d $device -s "White Balance Temperature, Auto" 0
uvcdynctrl -d $device -s "Exposure, Auto Priority" 0
